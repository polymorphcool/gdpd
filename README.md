# GodotPuredata

Custom module for godot engine to load and manage puredata patches

We are currently discussing functionalities and features of the module, so please push some issues with your requests and ideas.

## Topics:

- how deep should be the integration?
- how to edit pd patches while working in godot?
- how to handle project exports?
- wich license?
